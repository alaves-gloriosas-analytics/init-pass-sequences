#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 19 2020

@author: Sergio Llana (@SergioMinuto90)
"""


import matplotlib.patches as patches
import matplotlib.patheffects as pe
import matplotlib.pyplot as plt
import matplotlib.image as image
import numpy as np

from utils import read_json


config = read_json("visualization/plot_config.json")
height = float(config["height"])
width = float(config["width"])


def _point_to_meters(p):
    '''
    Convert a point's coordinates from a 0-1 range to meters.
    '''
    return np.array([p[0]*width, p[1]*height])


def _meters_to_point(p):
    '''
    Convert a point's coordinates from meters to a 0-1 range.
    '''
    return np.array([p[0]/width, p[1]/height])


def _change_range(value, old_range, new_range):
    '''
    Convert a value from one range to another one, maintaining ratio.
    '''
    return ((value-old_range[0]) / (old_range[1]-old_range[0])) * (new_range[1]-new_range[0]) + new_range[0]


def draw_pitch(min_x=0, max_x=1):
    """
    Plot an empty horizontal football pitch, returning Matplotlib's ax object so we can keep adding elements to it.

    Parameters
    -----------
        min_x: float value from 0 to 'max_x' to choose a subsection of the pitch. Default value is 0.
        max_x: float value from 'min_x' to 1 to choose a subsection of the pitch. Default value is 1.

    Returns
    -----------
       ax : Matplotlib's axis object to keetp adding elements on the pitch.
    """
    background_color = config["background_color"]
    lines_color = config["lines_color"]
    fig_size = config["fig_size"]

    # This allows to plot a subsection of the pitch
    ratio = height / float((width * max_x)-(width * min_x))
    f, ax = plt.subplots(1, 1, figsize=(fig_size, fig_size*ratio), dpi=100)

    ax.set_ylim([0, height])
    ax.set_xlim([width*min_x, width*max_x])
    ax.add_patch(patches.Rectangle((0, 0), width, height, color=background_color))

    im = image.imread('img/alaves.png')
    ax.imshow(im, aspect='auto', extent=(96, 102, 4, 10), zorder=10)

    # Plot outer lines
    line_pts = [
        [_point_to_meters([0, 0]), _point_to_meters([0, 1])],  # left line
        [_point_to_meters([1, 0]), _point_to_meters([1, 1])],  # right line
        [_point_to_meters([0, 1]), _point_to_meters([1, 1])],  # top line
        [_point_to_meters([0, 0]), _point_to_meters([1, 0])],  # bottom line
    ]

    for line_pt in line_pts:
        ax.plot([line_pt[0][0], line_pt[1][0]], [line_pt[0][1], line_pt[1][1]], 'w-',
                alpha=0.8, lw=1.5, zorder=3, color=lines_color)

    # Plot boxes
    line_pts = [
        [_point_to_meters([0.5, 0]), _point_to_meters([0.5, 1])],  # center line

        # left box
        [[0, 24.85], [0, 2.85]],
        [[0, 13.85], [16.5, 13.85]],
        [[0, 54.15], [16.5, 54.15]],
        [[16.5, 13.85], [16.5, 54.15]],

        # left goal
        [[0, 24.85], [5.5, 24.85]],
        [[0, 43.15], [5.5, 43.15]],
        [[5.5, 24.85], [5.5, 43.15]],

        # right box
        [[105, 24.85], [105, 2.85]],
        [[105, 13.85], [88.5, 13.85]],
        [[105, 54.15], [88.5, 54.15]],
        [[88.5, 13.85], [88.5, 54.15]],

        # right goal
        [[105, 24.85], [99.5, 24.85]],
        [[105, 43.15], [99.5, 43.15]],
        [[99.5, 24.85], [99.5, 43.14]]
    ]

    for line_pt in line_pts:
        ax.plot([line_pt[0][0], line_pt[1][0]], [line_pt[0][1], line_pt[1][1]], 'w-',
                alpha=0.8, lw=1.5, zorder=3, color=lines_color)

    # Plot circles
    ax.add_patch(patches.Wedge((94.0, 34.0), 9, 130, 230, fill=True, edgecolor=lines_color,
                               facecolor=lines_color, zorder=4, width=0.02, alpha=0.8))

    ax.add_patch(patches.Wedge((11.0, 34.0), 9, 310, 50, fill=True, edgecolor=lines_color,
                               facecolor=lines_color, zorder=4, width=0.02, alpha=0.8))

    ax.add_patch(patches.Wedge((52.5, 34), 9.5, 0, 360, fill=True, edgecolor=lines_color,
                               facecolor=lines_color, zorder=4, width=0.02, alpha=0.8))

    plt.axis('off')
    return ax


def draw_pass_sequence(ax, pass_sequence, title="", legend=""):

    background_color = config["background_color"]

    for index, row in pass_sequence.iterrows():
        if row.type == 'Pass':
            player_x = _statsbomb_to_point(row.location)[0]*width
            player_y = _statsbomb_to_point(row.location)[1]*height

            player_recv_x = _statsbomb_to_point(row.pass_end_location)[0]*width
            player_recv_y = _statsbomb_to_point(row.pass_end_location)[1]*height

            ax.plot(player_x, player_y, '.', color='blue', markersize=10, zorder=5)
            ax.plot([player_x, player_recv_x], [player_y, player_recv_y],
                    'w-', linestyle='-', alpha=1, lw=2, zorder=3, color='blue')
        elif row.type == 'Carry':
            location_x = _statsbomb_to_point(row.location)[0]*width
            location_y = _statsbomb_to_point(row.location)[1]*height

            end_location_x = _statsbomb_to_point(row.carry_end_location)[0]*width
            end_location_y = _statsbomb_to_point(row.carry_end_location)[1]*height

            ax.plot(location_x, location_y, '.', color='lime', markersize=10, zorder=5)
            ax.plot([location_x, end_location_x], [location_y, end_location_y],
                    'w-', linestyle='--', alpha=1, lw=2, zorder=3, color='lime')

    # Step 3: Extra information shown on the plot
    ax.annotate("@juanarmentia", xy=(0.99*width, 0.02*height),
                ha="right", va="bottom", zorder=7, fontsize=10, color=config["lines_color"])

    if legend:
        ax.annotate(legend, xy=(0.01*width, 0.02*height),
                    ha="left", va="bottom", zorder=7, fontsize=10, color=config["lines_color"])

    if title:
        ax.set_title(title, loc="left")

    return ax


def _statsbomb_to_point(location, max_width=120, max_height=80):
    """
    Convert a point's coordinates from a StatsBomb's range to 0-1 range.
    """
    return location[0] / max_width, 1-(location[1] / max_height)
