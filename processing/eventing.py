#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from abc import ABC, abstractmethod
from datetime import datetime
import pandas as pd
import numpy as np
import warnings
from statsbombpy import sb
from processing import PassingSequencekBuilder

warnings.simplefilter(action='ignore', category=pd.errors.PerformanceWarning)


class StatsbombPassingSequence(PassingSequencekBuilder, ABC):
    def __init__(self, args):
        self.team_name = args.team_name
        self.match_id = args.match_id
        # if args.range_minutes is not None:
        #     self.range_minutes = tuple(args.range_minutes)
        # else:
        #     self.range_minutes = None
        self.range_minutes = None

        self.plot_name = None
        self.df_events = None
        self.df_team_events = None
        self.plot_title = None
        self.names_dict = None
        self.plot_legend = None
        self.num_minutes = None
        self.player_position = None
        self.pass_seq_array = None

    def read_data(self):
        """
        Read StatsBomb eventing data of the selected 'match_id', generating a pandas DataFrame
        with the events and a dictionary of player names and nicknames.
        """
        self.df_events = sb.events(match_id=self.match_id)
        self.df_events.sort_values(['period', 'timestamp'], ascending=[True, True], inplace=True)
        self.df_events.index = list(np.arange(len(self.df_events)))

        # TODO: Ordenar eventos

        self.df_team_events = self.df_events[self.df_events.team == self.team_name]
        if self.range_minutes is not None:
            self.df_team_events = self.df_team_events[(self.df_team_events.minute >= self.range_minutes[0]) &
                                                      (self.df_team_events.minute < self.range_minutes[1])]
        self.df_team_events['cumsum'] = (self.df_team_events["possession"] !=
                                         self.df_team_events["possession"].shift()).cumsum()
        df_init_events = self.df_team_events.groupby('cumsum').first()
        df_init_events = (
            df_init_events[
                (df_init_events.position == 'Goalkeeper') &
                (df_init_events.type != 'Goal Keeper')
            ]
        )

        df_possessions_init = self.df_team_events[
            self.df_team_events.possession.isin(df_init_events.possession.unique())]

        self.pass_seq_array = []
        for possession in df_init_events.possession.unique():
            self.pass_seq_array.append(df_possessions_init[df_possessions_init.possession == possession].copy())

    def prepare_data(self):
        """
        Prepares the five pandas DataFrames that 'draw_pass_map' needs.
        """
        # We select all successful passes done by the selected team before the minute
        # of the first substitution or red card.
        df_passes = self.df_events[(self.df_events.type == "Pass") &
                                   (self.df_events.pass_outcome.isnull()) &
                                   (self.df_events.team == self.team_name) &
                                   (self.df_events.minute < self.num_minutes)].copy()

        # If available, use player's nickname instead of full name to optimize space in plot
        df_passes["pass_recipient"] = df_passes.pass_recipient.apply(
            lambda x: self.names_dict[x] if self.names_dict[x] else x)
        df_passes["player"] = df_passes.player.apply(
            lambda x: self.names_dict[x] if self.names_dict[x] else x)

        # We select all successful passes done by the selected team before the minute
        # of the first substitution or red card.
        df_carries = self.df_events[(self.df_events.type == "Carry") &
                                    (self.df_events.team == self.team_name)].copy()

    def set_text_info(self, n_pass_sequence):
        """
        Set the plot's name, title and legend information based on the customization chosen with the command line arguments.
        """
        # Name of the .PNG in the plots/ folder
        self.plot_name = "statsbomb_match{0}_{1}_{2}".format(self.match_id, self.team_name, datetime.now().timestamp())

        # Title of the plot
        opponent_team = [x for x in self.df_events.team.unique() if x != self.team_name][0]
        self.plot_title = "Secuencias de pases desde inicio del {0} contra el {1} ({2})" \
            .format(self.team_name, opponent_team, n_pass_sequence)

        # Information in the legend
        self.plot_legend = "Pases: azul\nConducciones: verde"

    @staticmethod
    def statsbomb_to_point(location, max_width=120, max_height=80):
        """
        Convert a point's coordinates from a StatsBomb's range to 0-1 range.
        """
        return location[0] / max_width, 1-(location[1] / max_height)
