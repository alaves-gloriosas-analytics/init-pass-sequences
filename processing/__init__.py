#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 19 2020

@author: Sergio Llana (@SergioMinuto90)
"""


import abc
from abc import ABC, abstractmethod
import matplotlib.pyplot as plt

from visualization.passing_sequence import draw_pitch, draw_pass_sequence


class PassingSequencekBuilder(ABC):
    """
    Abstract class that defines a template method containing a skeleton of
    the code for building a passing network plot.

    Concrete subclasses should implement these operations for specific data
    sources (e.g. eventing vs tracking).
    """
    def build_and_save(self):
        """
        Template of the algorithm.
        """
        self.read_data()
        self.prepare_data()
        self.build_plot()

        print("done!")

    @abstractmethod
    def read_data(self):
        pass

    @abstractmethod
    def set_text_info(self, n_pass_sequence):
        pass

    @abstractmethod
    def prepare_data(self):
        pass

    def build_plot(self):
        """
        Plot the pitch and passing network, saving the output image into the 'plots' folder.
        """
        index = 0
        for pass_sequence in self.pass_seq_array:
            ax = draw_pitch()
            self.set_text_info(index + 1)
            draw_pass_sequence(ax, pass_sequence, self.plot_title, self.plot_legend)
            plt.savefig("plots/{0}".format(self.plot_name) + str(index) + ".png")
            plt.clf()
            index += 1
