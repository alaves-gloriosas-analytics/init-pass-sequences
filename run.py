#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 19 2020

@author: Sergio Llana (@SergioMinuto90)
"""


from processing.eventing import StatsbombPassingSequence
from utils import parse_args


def main(args):
    plot_builder = StatsbombPassingSequence(args)
    plot_builder.build_and_save()


if __name__ == "__main__":
    parsed_args = parse_args()
    if parsed_args:
        main(parsed_args)
