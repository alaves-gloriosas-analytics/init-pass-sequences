#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 19 2020

@author: Sergio Llana (@SergioMinuto90)
@author: Laurie Shaw (@EightyFivePoint)
"""


import pandas as pd
import csv as csv
import argparse
import json
import sys


def parse_args():
    '''
    Parse command line arguments for plot customization
    '''
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--match-id', dest='match_id', help='Match ID', required=True)
    parser.add_argument('-t', '--team-name', dest='team_name', help='Selected team in match', required=True)
    args = parser.parse_args(sys.argv[1:])

    return args


def read_json(path):
    '''
    Read JSON file from path
    '''
    return json.loads(read(path))


def read(path):
    '''
    Read content of a file
    '''
    with open(path, 'r') as f:
        return f.read()


def to_single_playing_direction(home, away, events):
    '''
    Flip coordinates in second half so that each team always shoots in the same direction through the match.
    '''
    for team in [home, away, events]:
        second_half_idx = team.Period.idxmax(2)
        columns = [c for c in team.columns if c[-1].lower() in ['x', 'y']]
        team.loc[second_half_idx:, columns] = team.loc[second_half_idx:, columns].apply(lambda x: 1-x, axis=1)

    return home, away, events


"""
----------------------
Laurie's methods below
----------------------
"""
def read_event_data(DATADIR, game_id):
    '''
    read_event_data(DATADIR,game_id):
    read Metrica event data  for game_id and return as a DataFrame
    '''
    eventfile = 'Sample_Game_%s/Sample_Game_%s_RawEventsData.csv' % (game_id, game_id)  # filename
    events = pd.read_csv('{}/{}'.format(DATADIR, eventfile))  # read data
    return events
